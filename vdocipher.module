<?php

/**
 * @file
 * A plugin module that parses content detect vdo shortcodes and render the corresponding video player
 */

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */
function vdocipher_help($path, $arg){
	switch ($path) {
		case 'admin/help#vdocipher':
			return '<p>' . t("Embeds your vdocipher video inside Drupal nodes.") . '</p>';
			break;

		case 'admin/config/content/vdocipher':
			return '<p>' . t("Add API key to complete module set up") . '</p>';
			break;
	}
}

function vdocipher_node_view($node, $view_mode, $langcode)
{
	if (!is_array($node->content)) {
		return true;
	}
	foreach ($node->content as $key=>&$value) {
		if ($key != 'body' && !preg_match('/^field_/', $key)) {
			continue;
		}
		if (!isset($value['#items'])) {
			continue;
		}
		for ($i=0; $i<count($value['#items']); $i++) {
			if (isset($value[$i]['#markup'])) {
				vdo_shortcode_process($value[$i]['#markup']);
			}
		}
	}
}

function vdo_shortcode_process( &$text ) {
	$text = preg_replace_callback( '/\[vdo\s+([A-Za-z0-9\=\s]+)\]/' , function($matches){
		if (is_null(variable_get('vdocipher_api_key'))) {
			return "Plugin not configured. Please enter API key.";
		}
		$access_key = variable_get('vdocipher_api_key');
		$attrs = [];
		$output = preg_replace_callback( '/\b([a-zA-Z0-9]+)\=([A-Za-z0-9]+)\b/' , function($matches) use(&$attrs){
				$attrs[$matches[1]] = $matches[2];
			} , $matches[1]);
		$params = array(
			'video'=>$attrs['id'],
		);
		$height = (isset($attrs['height'])) ? $attrs['height'] : variable_get('vdo_default_height' , 720);
		$width = (isset($attrs['width'])) ? $attrs['width'] : variable_get('vdo_default_width' , 1280);
    $player_version = variable_get('vdo_player_version','1.6.7');
    $player_theme = variable_get('vdo_player_theme','9ae8bbe8dd964ddc9bdb932cca1cb59a');
    $url = "https://dev.vdocipher.com/api/videos/".$params['video']."/otp";
		$headers = array(
      'Authorization'=>'Apisecret '.$access_key,
      'Content-Type'=>'application/json',
      'Accept'=>'application/json'
		);
    $otp_post_array = array("ttl" => 300);
    $otp_post_array["annotate"] = variable_get('watermark_code');

    $otp_post_json = json_encode($otp_post_array);
		$response = drupal_http_request($url, array(
			'headers'	=>	$headers,
			'method'	=>	'POST',
			'data'		=>	$otp_post_json,
			'max_redirects'=>	2
		));
    $OTP_Response =  $response->data;
    $OTP_decoded = drupal_json_decode($OTP_Response);
    if ($response->code != 200 && isset($OTP_decoded['message'])) {
     return $OTP_decoded['message'];
    }
    if (is_null($OTP_decoded)|| !isset($OTP_decoded['otp'])) {
      return "Video playback can not be authenticated." ;
    }

    $OTP = $OTP_decoded['otp'];
    $playbackInfo = $OTP_decoded['playbackInfo'];

$output = <<< EOF
  <div id="vdo$OTP" style="width: 1280px; max-width:100%; height:auto;"></div>
  <script>
    (function(v,i,d,e,o){v[o]=v[o]||{}; v[o].add = v[o].add || function V(a){
    (v[o].d=v[o].d||[]).push(a); };if(!v[o].l) { v[o].l=1*new Date();
    a=i.createElement(d), m=i.getElementsByTagName(d)[0]; a.async=1; a.src=e;
    m.parentNode.insertBefore(a,m); }})(window,document,"script",
    "https://cdn-gce.vdocipher.com/playerAssets-staging/$player_version/vdo.js","vdo");
    vdo.add({
      otp: "$OTP",
      playbackInfo: "$playbackInfo",
      theme: "$player_theme",
      container: document.querySelector( "#vdo$OTP"  ),
    });
  </script>
EOF;

		return $output;
	}, $text );
}

/**
 * Implements hook_menu().
 */
function vdocipher_menu() {
  $items = array();

  $items['admin/config/content/vdocipher'] = array(
    'title' => 'VdoCipher Plugin Settings',
    'description' => 'Configuration for VdoCipher module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vdocipher_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}


/**
 * Page callback: VdoCipher settings
 *
 * @see vdocipher_menu()
 */
function vdocipher_form($form, &$form_state) {
  $form['vdocipher_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Secret Key'),
    '#default_value' => variable_get('vdocipher_api_key'),
    '#size' => 68,
    '#minlength' => 64,
    '#maxlength' => 64,
    '#description' => t('The API secret key for vdocipher account.'),
    '#required' => TRUE,
  );
  $form['vdo_default_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Default width'),
    '#default_value' => variable_get('vdo_default_width', 1280),
    '#size' => 8,
    '#minlength' => 2,
    '#maxlength' => 4,
    '#description' => t('Default width of video.'),
    '#required' => TRUE,
  );
  $form['vdo_default_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Default height'),
    '#default_value' => variable_get('vdo_default_height', 720),
    '#size' => 8,
    '#maxlength' => 4,
    '#description' => t('Default height of video.'),
    '#required' => TRUE,
  );
  $form['watermark_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Watermark code'),
    '#default_value' => variable_get('watermark_code'),
    '#rows' => 4,
    '#columns' => 4,
    '#description' => t('Watermark code. This needs to be in a structured format as described in https://www.vdocipher.com/blog/2014/12/add-text-to-videos-with-watermark/'),
    '#required' => FALSE,
  );
  $form['vdo_player_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Version'),
    '#default_value' => variable_get('vdo_player_version', '1.6.7'),
    '#size' => 8,
    '#minlength' => 2,
    '#maxlength' => 8,
    '#description' => t('Video Player Version'),
    '#required' => TRUE,
  );
  $form['vdo_player_theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Video Player Theme'),
    '#default_value' => variable_get('vdo_player_theme', '9ae8bbe8dd964ddc9bdb932cca1cb59a'),
    '#size' => 40,
    '#minlength' => 8,
    '#maxlength' => 64,
    '#description' => t('Video Player theme value'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
# vim: set filetype=php; set syntax=php; set expandtab; set tabstop=2; set shiftwidth=2
